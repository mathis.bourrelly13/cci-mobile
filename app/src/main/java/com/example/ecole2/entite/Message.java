package com.example.ecole2.entite;
public class Message {
    private int id;
    private String email;
    private String message;
    private String nom;
    private String prenom;

    public Message(){}
    public Message(int id, String email, String message, String nom, String prenom) {
        this.id = id;
        this.email = email;
        this.message = message;
        this.nom = nom;
        this.prenom = prenom;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getMessage() {
        return message;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }
}
