package com.example.ecole2.entite;

import java.util.List;

public class Formation {
    private String acronyme;
    private String dateDebut;
    private int dureeMois;
    private String intitule;
    private String link;
    private String videoUrl;
    private String description;

    public Formation(){};

    public Formation(String acronyme, String dateDebut, int dureeMois, String intitule, String link, String videoUrl, String description) {
        this.acronyme = acronyme;
        this.dateDebut = dateDebut;
        this.dureeMois = dureeMois;
        this.intitule = intitule;
        this.link = link;
        this.videoUrl = videoUrl;
        this.description = description;
    }


    public String getAcronyme() {
        return acronyme;
    }

    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public int getDureeMois() {
        return dureeMois;
    }

    public void setDureeMois(int dureeMois) {
        this.dureeMois = dureeMois;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
