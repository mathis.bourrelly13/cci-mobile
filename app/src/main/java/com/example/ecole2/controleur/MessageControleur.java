package com.example.ecole2.controleur;

import android.util.Log;

import com.example.ecole2.entite.Message;
import com.example.ecole2.model.api.IResponseRestCallback;
import com.example.ecole2.model.api.MessageRepository;
import com.example.ecole2.model.api.RequeteAsynchrone;
import com.example.ecole2.vue.MessageActivity;

public class MessageControleur {
    private static String TAG = "ControleMessage";
    private static MessageControleur instance = null ;
    private Message message;
    private static RequeteAsynchrone requeteAsynchrone;

    public MessageControleur(){
        super();
    }

    public static MessageControleur getInstance(){
        if(instance == null){
            instance = new MessageControleur();
            requeteAsynchrone = RequeteAsynchrone.getInstance();
            Log.i(TAG, "constructeur");
        }
        return MessageControleur.instance;
    }


    public void sendMessage(Message message) {
        Log.i(TAG, "setMessage");
        MessageRepository.getInstance().envoyerMessage(message, RequeteAsynchrone.getInstance());
    }
}