package com.example.ecole2.controleur;

import android.util.Log;

import com.example.ecole2.model.api.RequeteAsynchrone;
import com.example.ecole2.entite.Formation;

import java.util.ArrayList;
import java.util.List;

public class FormationControleur {


    private static String TAG = "ControleEtudiant";
    private static FormationControleur instance = null ;
    private List<Formation> formations = new ArrayList<>();
    private Formation formation;
    private static RequeteAsynchrone requeteAsynchrone;

    public FormationControleur(){
        super();
    }

    public static FormationControleur getInstance(){
        if(instance == null){
            instance = new FormationControleur();
            requeteAsynchrone = RequeteAsynchrone.getInstance();
            Log.i(TAG, "constructeur");
        }
        return FormationControleur.instance;
    }

    public Formation getFormationByAcronyme(String acronyme) {
        for (Formation I: getFormations()) {
            boolean compare = I.getAcronyme().equals(acronyme);
            if (compare) {
                return I;
            }
        }
        return null;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    public List<Formation> getFormations() {
        return formations;
    }

    public void setFormations(List<Formation> formations) {
        Log.i(TAG, "setEtudiants");
        this.formations = formations;
    }

}
