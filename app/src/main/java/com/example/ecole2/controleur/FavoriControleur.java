package com.example.ecole2.controleur;

import android.content.Context;
import android.util.Log;

import com.example.ecole2.model.database.FavoriRepository;
import com.example.ecole2.entite.Formation;

import java.util.ArrayList;
import java.util.List;

public class FavoriControleur {
    private static String TAG = "ControleFavori";
    private static FavoriControleur instance = null ;
    private static List<Formation> favoriList = new ArrayList<>();
    private FavoriRepository favoriRepository = new FavoriRepository();
    private FormationControleur formationControleur = new FormationControleur();
    private static boolean updater = true;

    private FavoriControleur() { super(); }

    public static FavoriControleur getInstance(){
        if(instance == null){
            instance = new FavoriControleur();
            Log.i(TAG, "constructeur");
        }
        return FavoriControleur.instance;
    }

    public List<Formation> getFavoriList() {
        return favoriList;
    }

    public void addFavoriToList(Formation formation) {
        favoriRepository.putFav(formation);
        favoriList.add(formation);
    }

    public void removeFavoriToList(Formation formation) {
        favoriRepository.removeFav(formation);
        favoriList.remove(formation);
    }



    public Formation getFavoriFromList(int index) {
        return favoriList.get(index);
    }

    public void fillList(Context context) {
        if (updater){
            for (String I : favoriRepository.getInstance(context).getAllFavAcronyme()) {
                favoriList.add(formationControleur.getInstance().getFormationByAcronyme(I));
            }
        }
        updater = false;
    }
}
