package com.example.ecole2.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidatorHelper {
    public boolean isValidEmail(String string){
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
    public boolean isOnlyCharater(String string){
        final String LETTER_PATTERN = "[a-zA-Zéèçàêêî]+";
        Pattern pattern = Pattern.compile(LETTER_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
    public boolean isNullOrEmpty(String string){
        if(string == null) return true;
        else if(string.isEmpty()) return true;
        else return false;
        //return TextUtils.isEmpty(string);
    }

    public boolean isNumeric(String string){
        //return TextUtils.isDigitsOnly(string);
        final String LETTER_PATTERN = "[0-9.]+";
        Pattern pattern = Pattern.compile(LETTER_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
