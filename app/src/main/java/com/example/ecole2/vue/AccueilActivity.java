package com.example.ecole2.vue;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDelegate;

import com.example.ecole2.R;
import com.example.ecole2.controleur.FormationControleur;
import com.example.ecole2.model.database.FavoriRepository;


public class AccueilActivity extends RacineActivity {
    private static String TAG = "AccueilActivity";
    private FormationControleur formationControleur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_accueil);
        Log.i(TAG, "onCreate");
        Button btnVideo=(Button)findViewById(R.id.boutonVideoId);
        formationControleur = FormationControleur.getInstance();

        btnVideo.setOnClickListener(new Button.OnClickListener() {


            @Override
            public void onClick(View v) {
                Log.i(TAG,"Bouton video");
                Intent intent = new Intent(AccueilActivity.this, VideoActivity.class);
                intent.putExtra("urlVideo", "tt0DFNMJEzI");
                startActivity(intent);
            }
        });
    }
    public void enventListenerLink(View view) {
        Log.i(TAG,"myMethod");
        Log.i(TAG,"Bouton link");
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.cci.fr/"));
        startActivity(browserIntent);
    }
}