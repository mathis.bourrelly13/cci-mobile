package com.example.ecole2.vue;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ecole2.R;
import com.example.ecole2.controleur.FavoriControleur;
import com.example.ecole2.controleur.FormationControleur;
import com.example.ecole2.entite.Formation;

import java.util.List;

public class FavActivity extends RacineActivity implements AdapterView.OnItemClickListener{
    private String TAG = "FavActivity";
    private ListView liste;
    private List<Formation> formations;
    private FavoriControleur favoriControleur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);
        liste = (ListView) findViewById(R.id.listView);
        favoriControleur = FavoriControleur.getInstance();
        favoriControleur.fillList(this);
        formations = favoriControleur.getFavoriList();
        FormationAdapter formationAdapter = new FormationAdapter(this, formations);
        liste.setTextFilterEnabled(true);
        liste.setAdapter(formationAdapter);
        liste.setOnItemClickListener(this);

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "onItemClick");
        Formation formation = FavoriControleur.getInstance().getFavoriFromList(position);
        Log.i(TAG, "onItemClick formation=" + formation.getAcronyme());
        FormationControleur.getInstance().setFormation(formation);
        Intent intent = new Intent(FavActivity.this, VoirFormationActivity.class);
        startActivity(intent);
    }
}
