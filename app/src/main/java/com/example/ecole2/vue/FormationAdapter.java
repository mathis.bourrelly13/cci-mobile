package com.example.ecole2.vue;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ecole2.R;
import com.example.ecole2.entite.Formation;
import com.example.ecole2.util.Outils;

import java.util.List;

public class FormationAdapter extends BaseAdapter {
	private Context mContext;
	private List<Formation> mFormations;
	private LayoutInflater inflater;
	// Store the list of image planete
	public FormationAdapter(Context c, List<Formation> ids) {
		mContext = c;
		mFormations = ids;
		inflater = LayoutInflater.from(c);
	}
	// Return the number of items in the Adapter
	@Override
	public int getCount() {
		if (mFormations != null)
		{
			return mFormations.size();
		}
		return 0;
	}
	// Return the data item at position
	@Override
	public Object getItem(int position) {
		return mFormations.get(position);
	}
	// Will get called to provide the ID that
	// is passed to OnItemClickListener.onItemClick()
	@Override
	public long getItemId(int position) {
		return position;
	}
	// Return an ImageView for each item referenced by the Adapter
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textIntitule;
		TextView textDureeMois;
		TextView textdateDebut;
        View view;

        Formation formation = mFormations.get(position);
        //creations d'une nouvelle view de la liste
        if(convertView==null) {
            view = inflater.inflate(R.layout.view_formation, null);
        }
        else {
            view = convertView;           
        }
		textIntitule = (TextView) view.findViewById(R.id.intituleId);
		textIntitule.setText(formation.getIntitule());
		textDureeMois = (TextView) view.findViewById(R.id.duree_moisId);
		textDureeMois.setText(String.valueOf(formation.getDureeMois())+" mois");
		textdateDebut = (TextView) view.findViewById(R.id.date_debutId);
		textdateDebut.setText(String.valueOf("débute le " + Outils.convertStringDate(formation.getDateDebut())));
        return view;
	}

}
