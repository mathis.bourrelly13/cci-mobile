package com.example.ecole2.vue;

import static com.example.ecole2.util.Constante.YoutubeUrlImageBase;
import static com.example.ecole2.util.Constante.YoutubeUrlImageEnd;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.ecole2.R;
import com.example.ecole2.controleur.FavoriControleur;
import com.example.ecole2.controleur.FormationControleur;
import com.example.ecole2.model.database.DatabaseOpenHelper;
import com.example.ecole2.model.database.FavoriRepository;
import com.example.ecole2.entite.Formation;
import com.example.ecole2.util.Outils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;


public class VoirFormationActivity extends RacineActivity {
    private final String TAG = "FormtationActivity";
    //private DatabaseOpenHelper mDbHelper;
    private SimpleCursorAdapter mAdapter;
    private Formation formation;
    private boolean checked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "create formation");
        super.onCreate(savedInstanceState);
        FavoriRepository.getInstance(this);

        setContentView(R.layout.activity_voir_formation);
        formation = FormationControleur.getInstance().getFormation();

        Log.i(TAG, "Formation=" + formation);
        TextView textViewAcronyme = findViewById(R.id.acronymeId);
        ImageView imageViewImage = findViewById(R.id.ImageId);
        TextView textViewNbDate_debut = findViewById(R.id.date_debutId);
        TextView textViewNbDuree_mois = findViewById(R.id.duree_moisId);
        TextView textViewIntitule = findViewById(R.id.intituleId);
        TextView textViewDescription = findViewById(R.id.descriptionId);
        TextView textViewLink = findViewById(R.id.linkId);
        ToggleButton toggleButtonFav = findViewById(R.id.favId);

        textViewAcronyme.setText(formation.getAcronyme());
        textViewNbDate_debut.setText(Outils.convertStringDate(formation.getDateDebut()));
        textViewNbDuree_mois.setText(String.valueOf(formation.getDureeMois()));
        textViewIntitule.setText(formation.getIntitule());
        loadImageView(imageViewImage, YoutubeUrlImageBase + formation.getVideoUrl() + YoutubeUrlImageEnd);
        textViewDescription.setText(formation.getDescription());
        textViewLink.setText(formation.getLink());

        List<Formation> favList = FavoriControleur.getInstance().getFavoriList();
        if(favList.contains(formation)){
            checked = true;
        }
        if (checked){
            toggleButtonFav.setChecked(true);
        }
        else{
            toggleButtonFav.setChecked(false);
        }
        imageViewImage.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Bouton video");
                Intent intent = new Intent(VoirFormationActivity.this, VideoActivity.class);
                intent.putExtra("urlVideo", formation.getVideoUrl());
                startActivity(intent);
            }});

        toggleButtonFav.setOnClickListener(new ToggleButton.OnClickListener(){

            @Override
            public void onClick(View v) {
                Log.i(TAG,"Bouton fav");
                if (checked){
                    FavoriControleur.getInstance().removeFavoriToList(formation);
                }
                else{
                    FavoriControleur.getInstance().addFavoriToList(formation);
                }
            }});
    }

    public void loadImageView(ImageView img, String url) {
        //start a background thread for networking
        Log.i("loadImageView", url);
        new Thread(new Runnable() {
            public void run() {
                try {
                    //download the drawable
                    final Drawable drawable = Drawable.createFromStream((InputStream) new URL(url).getContent(), "src");
                    //edit the view in the UI thread
                    img.post(new Runnable() {
                        public void run() {
                            img.setImageDrawable(drawable);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("==================erreur test===============","test",e);
                }

            }
        }).start();
    }

}
