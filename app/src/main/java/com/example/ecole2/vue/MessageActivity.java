package com.example.ecole2.vue;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ecole2.util.InputValidatorHelper;

import com.example.ecole2.R;
import com.example.ecole2.controleur.MessageControleur;
import com.example.ecole2.entite.Message;

public class MessageActivity extends RacineActivity {

    private String TAG = "MessageActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Message message = new Message();


        Log.i(TAG, "Message");
        EditText EditTextNom = findViewById(R.id.nomId);
        EditText EditTextprenom = findViewById(R.id.prenomId);
        EditText EditTextemail = findViewById(R.id.emailId);
        EditText EditTexttexte = findViewById(R.id.texteId);
        Button ButtonEnvoyer = findViewById(R.id.buttonId);


        ButtonEnvoyer.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "Bouton envoyer");
                message.setNom(String.valueOf(EditTextNom.getText()));
                message.setPrenom(String.valueOf(EditTextprenom.getText()));
                message.setEmail(String.valueOf(EditTextemail.getText()));
                message.setMessage(String.valueOf(EditTexttexte.getText()));
                InputValidatorHelper inputValidatorHelper = new InputValidatorHelper();

                if (!message.getNom().equals("")) {
                    if (!message.getPrenom().equals("")) {
                        if (!message.getEmail().equals("")) {
                            if (!message.getMessage().equals("")) {
                                if (inputValidatorHelper.isOnlyCharater(message.getNom())) {
                                    if (inputValidatorHelper.isOnlyCharater(message.getPrenom())) {
                                        if (inputValidatorHelper.isValidEmail(message.getEmail())) {
                                            if (inputValidatorHelper.isOnlyCharater(message.getMessage())) {
                                                MessageControleur.getInstance().sendMessage(message);
                                                Toast.makeText(MessageActivity.this, "Message envoyé !", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(MessageActivity.this, "Message Invalide!", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(MessageActivity.this, "E-mail Invalide!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(MessageActivity.this, "Prenom Invalide!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(MessageActivity.this, "Nom Invalide!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(MessageActivity.this, "Message manquant !", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MessageActivity.this, "E-mail manquant !", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MessageActivity.this, "Prenom manquant !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MessageActivity.this, "Nom manquant !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

