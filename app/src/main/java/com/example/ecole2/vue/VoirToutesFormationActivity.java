package com.example.ecole2.vue;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ecole2.R;
import com.example.ecole2.controleur.FormationControleur;
import com.example.ecole2.entite.Formation;

import java.util.List;

public class VoirToutesFormationActivity extends RacineActivity implements AdapterView.OnItemClickListener{
    private String TAG = "VoirToutesFormationActivity";
    private ListView liste;
    private List<Formation> formations;
    private FormationAdapter formationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formations = FormationControleur.getInstance().getFormations();
        setContentView(R.layout.activity_voir_toutes_formations);
        formationAdapter = new FormationAdapter(this, formations);
        liste = (ListView) findViewById(R.id.listView);
        liste.setTextFilterEnabled(true);
        liste.setAdapter(formationAdapter);
        liste.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "onItemClick");
        Formation formation = formations.get(position);
        FormationControleur.getInstance().setFormation(formation);
        Intent intent = new Intent(VoirToutesFormationActivity.this, VoirFormationActivity.class);
        startActivity(intent);
    }
}