package com.example.ecole2.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import com.example.ecole2.entite.Formation;
import com.example.ecole2.vue.FormationAdapter;

import java.util.ArrayList;

public class FavoriRepository {
    private static String TAG = "FavoryiRepository";
    private static FavoriRepository instance;
    private static Context FavActivity;
    private static DatabaseOpenHelper mDbHelper;

    public FavoriRepository() {
        super();
    }


    /*public DatabaseOpenHelper getmDbHelper() {
        return mDbHelper;
    }*/

    public static FavoriRepository getInstance(Context activity) {
        FavActivity = activity;
        if (instance == null) {
            instance = new FavoriRepository();
            Log.i(TAG, "constructeur");
        }
        mDbHelper = new DatabaseOpenHelper(FavActivity);
        return FavoriRepository.instance;
    }
    public static Cursor readAllFav() {
        return mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.TABLE_NAME,
                new String[]{DatabaseOpenHelper.FORMATION_NAME}, null, new String[]{}, null, null,
                null);
    }
    public static void putFav(Formation formation) {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.FORMATION_NAME, formation.getAcronyme());
        mDbHelper.getWritableDatabase().insert(DatabaseOpenHelper.TABLE_NAME, null, values);
    }

    public static void removeFav(Formation formation) {
        mDbHelper.getWritableDatabase().delete(DatabaseOpenHelper.TABLE_NAME,
                DatabaseOpenHelper.FORMATION_NAME + "=?",
                new String[] {formation.getAcronyme() });
    }

    public static ArrayList<String> getAllFavAcronyme() {
        ArrayList<String> acronymeFavoriList = new ArrayList<String>();
        Cursor cursor = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.TABLE_NAME,
                new String[]{DatabaseOpenHelper.FORMATION_NAME}, null, new String[]{}, null, null,
                null);
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                do {
                    // Get version from Cursor
                    try {
                        int n = cursor.getColumnIndex(mDbHelper.FORMATION_NAME);
                        String accronymeStr = cursor.getString(n);

                        acronymeFavoriList.add(accronymeStr);
                        Log.i(TAG, "formation ajouté : " + accronymeStr);
                    }
                    catch (SQLException e) {
                        Log.i(TAG, "Erreur lecture formations");
                    }
                } while (cursor.moveToNext());
            }
        }
        return acronymeFavoriList;
    }
}
