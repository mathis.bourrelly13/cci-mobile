package com.example.ecole2.model.api;

import android.util.Log;

import com.example.ecole2.controleur.FormationControleur;
import com.example.ecole2.entite.Formation;

import java.util.List;

public class RequeteAsynchrone implements IResponseRestCallback {
    private static String TAG = "RequeteAsynchrone";
    private FormationControleur formationController;
    private ServiceRestItf serviceRest;
    private static RequeteAsynchrone instance;

    public static RequeteAsynchrone getInstance(){
        if(instance == null){
            instance = new RequeteAsynchrone();
        }
        return instance;
    }

    private RequeteAsynchrone(){
        formationController = FormationControleur.getInstance();
        serviceRest = FormationRepository.getInstance();
        serviceRest.lireTousFormation(this);
        Log.i(TAG,"constructeur");
    }

    @Override
    public void responseRestCallback(Object reponse, int identifiantRequete) {
        if(identifiantRequete == 1){
            Log.i(TAG,"réponse formations");
            List<Formation> formations = (List<Formation>) reponse;
            Log.i(TAG,"réponse formations=" + formations);
            formationController.setFormations(formations);
        }
    }
}
