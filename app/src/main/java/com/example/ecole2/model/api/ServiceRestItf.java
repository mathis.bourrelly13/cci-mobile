package com.example.ecole2.model.api;

import com.example.ecole2.entite.Formation;
import com.example.ecole2.entite.Message;

public interface ServiceRestItf {
    void lireTousFormation(IResponseRestCallback objetReponse);
    void lireFormation(String acronyme, IResponseRestCallback objetReponse);
    void creerFormation(Formation formation, IResponseRestCallback objetReponse);
}
