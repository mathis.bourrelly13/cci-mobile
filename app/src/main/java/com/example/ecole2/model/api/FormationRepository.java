package com.example.ecole2.model.api;

import android.util.Log;

import com.example.ecole2.entite.Formation;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;


public class FormationRepository implements ServiceRestItf{

    private static final String TAG = "FormationRepository";
    private static IServiceRest serviceRestApi;
    private static IResponseRestCallback reponse;
    private static FormationRepository instance = new FormationRepository();

    public static FormationRepository getInstance(){
        return instance;
    }
    private FormationRepository(){
        Log.i(TAG, "========= initServiceRest ========");

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(IServiceRest.ENDPOINT)
                .setLog(new AndroidLog("retrofit"))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        serviceRestApi = adapter.create(IServiceRest.class);
    }

    public void lireTousFormation(IResponseRestCallback objetReponse){
        FormationRepository.reponse = objetReponse;
        serviceRestApi.getFormation(new Callback<List<Formation>>() {
            @Override
            public void success(List<Formation> fomrations, Response response) {
                Log.i(TAG, "========= getFormation all formations = "+fomrations);
                reponse.responseRestCallback(fomrations, 1);
            }
            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "========= getFormation all ERROR ======= error="+error);
            }
        });
    }

    public void lireFormation(String acronyme, IResponseRestCallback objetReponse){
        FormationRepository.reponse = objetReponse;
        serviceRestApi.getFormation(acronyme, new Callback<Formation>() {
            @Override
            public void success(Formation formation, Response response) {
                Log.i(TAG, "========= getPromotion promotion = "+formation);
                reponse.responseRestCallback(formation, 2);
            }
            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "========= getPromotion ERROR ======= error="+error);
            }
        });
    }

    public void creerFormation(Formation promotion, IResponseRestCallback objetReponse) {
        FormationRepository.reponse = objetReponse;
        serviceRestApi.creerFormation(promotion,new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                Log.i(TAG, "========= creerPromotion OK");
                reponse.responseRestCallback(null, 3);
            }
            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "========= creerPromotion ERROR ======= error="+error);
            }
        });
    }

}
