package com.example.ecole2.model.api;

import com.example.ecole2.entite.Formation;
import com.example.ecole2.entite.Message;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface IServiceRest {
    // serveur public
    public static final String ENDPOINT = "http://82.65.254.14:8089/ecole";
    // émulateur android studio
    //public static final String ENDPOINT = "http://10.0.2.2:8089/ecole";

    //public static final String ENDPOINT = "http://10.0.2.2:8080/8EtudiantPromotionPresentationCompteUtilisateur/rest";
    // émulateur geny motion
    //public static final String ENDPOINT = "http://10.0.3.2:8080/5EtudiantPromotion1vuePresentationRestWs/rest";
    @GET("/formation")
    public void getFormation(Callback<List<Formation>> formations);
    @GET("/formation/{acronyme}")
    public void getFormation(@Path("acronyme") String acronyme, Callback<Formation> formation);
    @POST("/formation")
    public void creerFormation(@Body Formation formation, Callback<Void> callback);
    @POST("/message")
    public void envoyerMessage(@Body Message message, Callback<Void> callback);
}
