
package com.example.ecole2.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
	
	public static final String TABLE_NAME = "formation";
	public static final String FORMATION_NAME = "acronyme";
	public static final String[] columns = { FORMATION_NAME };

	private static final String CREATE_CMD =

	"CREATE TABLE formation (" + FORMATION_NAME
			+ " VARCHAR(255) PRIMARY KEY)";


	private static final String NAME = "formation_db";
	private static final Integer VERSION = 1;
	private final Context mContext;

	public DatabaseOpenHelper(Context context) {
		super(context, NAME, null, VERSION);
		this.mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CMD);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// N/A
	}

	void deleteDatabase() {
		mContext.deleteDatabase(NAME);
	}
}
