package com.example.ecole2.model.api;

import android.util.Log;

import com.example.ecole2.entite.Message;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;

public class MessageRepository {
    private static final String TAG = "MessageRepository";
    private static IServiceRest serviceRestApi;
    private static IResponseRestCallback reponse;
    private static MessageRepository instance = new MessageRepository();

    public static MessageRepository getInstance(){
        return instance;
    }
    private MessageRepository(){
        Log.i(TAG, "========= initServiceRest ========");

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(IServiceRest.ENDPOINT)
                .setLog(new AndroidLog("retrofit"))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        serviceRestApi = adapter.create(IServiceRest.class);
    }

    public void envoyerMessage(Message message, IResponseRestCallback objetReponse) {
        MessageRepository.reponse = objetReponse;
        serviceRestApi.envoyerMessage(message,new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                Log.i(TAG, "========= envoyerMessage OK");
                reponse.responseRestCallback(null, 3);
            }
            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "========= envoyerMessage ERROR ======= error="+error);
            }
        });
    }
}
